import json
import csv
import os
import numpy as np
import pandas as pd
from scripts.node_features_utils import load_books_features

books_description_features ="data/books_description_features.npy"

def read_user_mappings(file_path: str):
    """
    Function that collects all user mappings
    :param file_path: file containing user mappings
    :return dict: dictionary with user mappings
    """
    with open(file_path, 'r', encoding='utf-8') as file:
        # skip header
        next(file)
        reader = csv.DictReader(file, fieldnames=['user_id_csv', 'user_id'])
        user_mapping_dict = dict()
        for row in reader:
            user_mapping_dict.setdefault(row['user_id'], row['user_id_csv'])

    return user_mapping_dict


def read_book_mappings(file_path: str):
    """
    Function that collects all book mappings

    :param file_path: file containing book mappings
    :return dict: dictionary with book mappings
    """
    with open(file_path, 'r', encoding='utf-8') as file:
        # skip header
        next(file)
        reader = csv.DictReader(file, fieldnames=['book_id_csv', 'book_id'])
        book_mapping_dict = dict()
        for row in reader:
            book_mapping_dict.setdefault(row['book_id'], row['book_id_csv'])

    return book_mapping_dict


def read_json_file(fp):
    """
    Function that reads json file with generators
    :param fp: json file
    :return:
    """
    while True:
        line = fp.readline()

        if line is None or len(line) == 0:
            break
        line = json.loads(line, encoding="utf-8")

        yield line


def extract_books_data_util(lines, columns: list):
    """
    Function that extracts given columns from a line w/ generators
    :param lines: lines with books data
    :param columns: columns that need to be extracted
    :return:
    """
    for line in lines:
        data_dict = dict()
        if line["language_code"] == "eng" and line["description"].strip():
            for column in columns:
                data_dict[column] = line[column]

            yield data_dict


def extract_reviews_data_util(lines, columns: list, book_ids: dict):
    """
    Function that extracts reviews for given book identification keys

    :param lines: lines with review data from users to books
    :param columns: columns that need to be extracted
    :param book_ids: books identification keys
    :return:
    """
    for line in lines:
        data_dict = dict()
        book_id = line['book_id']

        if book_id in book_ids.keys():
            for column in columns:
                data_dict[column] = line[column]

            yield data_dict


def extract_and_load_books(books_file_path: list, columns: list):
    """
    Function that collects books data, writes it in a new file
    and return dictionary with book ids for the purpose of knowing which books we had read

    :param books_file_path:  list of books filepaths
    :param columns: list of columns that need to be extracted
    :return dict: dictionary that contains books identification key
    """
    book_ids = dict()

    for filepath in books_file_path:
        dest_filepath = os.path.splitext(filepath)[0] + '_extracted.csv'

        with open(filepath, 'r', encoding='utf-8') as fp, open(dest_filepath, 'w', encoding='utf-8') as dest_file:
            lines = read_json_file(fp)
            lines = extract_books_data_util(lines, columns)

            writer = csv.writer(dest_file, lineterminator='\n')
            writer.writerow(columns)
            for line in lines:
                book_ids.setdefault(line['book_id'], 1)

                writer.writerow(line.values())

    return book_ids


def extract_reviews(books_file_path: list, book_ids: dict):
    """
    Function that collects all review data for given books
    and then writes all that data in new file

    :param books_file_path: list of books file paths where the review data is stored
    :param book_ids: book identification keys
    :return:
    """
    dest_file_paths = list()

    for filepath in books_file_path:
        columns = ["user_id", "book_id", "rating", "review_text", "n_votes", "date_added"]
        dest_filepath = os.path.splitext(filepath)[0] + '_extracted.csv'
        dest_file_paths.append(dest_filepath)

        with open(filepath, 'r', encoding='utf-8') as fp, open(dest_filepath, 'w', encoding='utf-8') as dest_file:
            lines = read_json_file(fp)
            lines = extract_reviews_data_util(lines, columns, book_ids)

            writer = csv.writer(dest_file, lineterminator='\n')
            writer.writerow(columns)
            for line in lines:
                writer.writerow(line.values())

    extract_year_column_reviews(dest_file_paths)


def extract_year_column_reviews(file_paths: list):
    for file in file_paths:
        df = pd.read_csv(file)
        df['date_added'] = pd.to_datetime(df['date_added'])
        df['date_added'] = df['date_added'].astype(str)
        df['date_added'] = df['date_added'].str.split(" ").str[0]
        df['date_added'] = pd.to_datetime(df['date_added'])
        df['year'] = df['date_added'].dt.year
        df.to_csv(file, encoding='utf-8', index=False)


def calculate_helpfulness_formula(votes_array: np, ratings_array: np):
    width = 4
    start = 1
    norm_votes = (votes_array - votes_array.min()) / votes_array.ptp() * width + start

    helpfulness = norm_votes + (ratings_array ** 2)

    width = 4
    start = 1
    norm_helpfulness = (helpfulness - helpfulness.min()) / helpfulness.ptp() * width + start
    norm_helpfulness = np.round(norm_helpfulness, 1).astype(int)

    return norm_helpfulness


def calculate_helpfulness_weights(reviews_file_path: list):
    for filepath in reviews_file_path:
        filepath = os.path.splitext(filepath)[0] + '_extracted.csv'

        df = pd.read_csv(filepath)
        df = df[df['rating'] > 0]
        df = df.drop(df[df['n_votes'] > 40].index)
        df = df.drop(df[df['n_votes'] < 0].index)

        votes_array = df['n_votes'].to_numpy()
        ratings_array = df['rating'].to_numpy()

        norm_helpfulness = calculate_helpfulness_formula(votes_array, ratings_array)
        df['helpfulness'] = norm_helpfulness
        filepath = os.path.splitext(filepath)[0] + '_with_weights.csv'
        df.to_csv(filepath, encoding='utf-8', index=False)


def extract_data(books_file_path: list, books_columns: list, reviews_file_path: list,
                 calculate_helpfulness: bool):
    """
    Function that extracts books and users data

    :param books_file_path: list of book file paths, from where we want to extract data
    :param books_columns: list of columns that we want to extract
    :param reviews_file_path: list of book reviews file paths, from where we want to extract reviews data
    :param calculate_helpfulness: true if you want to calculate review helpfulness

    :return:
    """
    # book_mappings = read_book_mappings(book_mappings_filepath)
    # user_mappings = read_user_mappings(user_mappings_filepath)

    book_ids = extract_and_load_books(books_file_path, books_columns)
    extract_reviews(reviews_file_path, book_ids)

    if calculate_helpfulness:
        calculate_helpfulness_weights(reviews_file_path)


def extract_and_clean_users_shelves_data(interaction_file_paths: list):
    dest_file_paths = list()
    for interaction_file in interaction_file_paths:
        extract_users_shelves_data(interaction_file)

        dest_filepath = os.path.splitext(interaction_file)[0] + '_extracted.csv'
        dest_file_paths.append(dest_filepath)

        df = pd.read_csv(dest_filepath, dtype={'user_id': str, 'book_id': str})
        df = df.dropna()
        df = df.drop_duplicates()

        # df = df.loc[df['is_read'] == True]

        book_features = load_books_features(books_description_features)
        df = df.loc[df['book_id'].isin(list(book_features.keys()))]

        df.to_csv(dest_filepath, encoding='utf-8', index=False)
    extract_year_column_reviews(dest_file_paths)


def extract_users_shelves_data(interaction_file_path: str):
    columns = ['user_id', 'book_id', 'is_read', 'date_added']
    dest_filepath = os.path.splitext(interaction_file_path)[0] + '_extracted.csv'

    with open(interaction_file_path, "r", encoding='utf-8') as file, open(dest_filepath, 'w',
                                                                          encoding='utf-8') as dest_file:
        lines = read_json_file(file)
        lines = extract_user_shelves_data_util(lines, columns)

        writer = csv.writer(dest_file, lineterminator='\n')
        writer.writerow(columns)
        for line in lines:
            writer.writerow(line.values())


def extract_user_shelves_data_util(lines, columns: list):
    for line in lines:
        data_dict = dict()

        for column in columns:
            data_dict[column] = line[column]

            yield data_dict
