import os
import json, csv
import pandas as pd
import numpy as np
from sentence_transformers import SentenceTransformer

YEAR = 2015


def load_books_features(features_file_path):
    """
    Function that loads books description features
    :param features_file_path: location of the books description features
    :return features_dict:
    """
    if not features_file_path or len(features_file_path) == 0:
        print("No filepath specified!")
        return

    with open(features_file_path, 'rb') as file:
        features = np.load(file, allow_pickle=True)

    features_dict = {}
    for book in features:
        book_id = str(book[0])
        book_features = list(book[1])
        features_dict.setdefault(book_id, book_features)

    return features_dict


def load_users_features(features_file_path):
    """
    Function that loads users description features
    :param features_file_path: location of the users description features
    :return features_dict:
    """
    if not features_file_path or len(features_file_path) == 0:
        print("No filepath specified!")
        return

    with open(features_file_path, 'rb') as file:
        features = np.load(file, allow_pickle=True)

    features_dict = {}
    for user in features:
        user_id = user[0]
        user_features = user[1]
        features_dict.setdefault(user_id, user_features)

    return features_dict


def generate_books_description_features(books_csv_file_paths: list, features_destination_path: str):
    """
    Function that generate description features for given books and writes them in numpy file
    :param books_csv_file_paths: list of books file paths
    :param features_destination_path: destination path where we want to store the features
    :return:
    """
    model = SentenceTransformer('paraphrase-MiniLM-L6-v2')
    books = pd.concat((pd.read_csv(f) for f in books_csv_file_paths))

    books_descriptions = books[["book_id", "description"]]

    sentences = np.array(books_descriptions['description'])
    sentence_embeddings = model.encode(sentences)

    # mapping in the following way (book_id, sentence_embeddings)
    books_embeddings = list()
    for book_id, sentence in zip(books['book_id'], sentence_embeddings):
        books_embeddings.append(np.array([book_id, sentence], dtype=object))

    books_embeddings = np.array(books_embeddings)

    with open(features_destination_path, 'wb') as file:
        np.save(file, books_embeddings)


def generate_books_reviews_features(book_reviews_csv_file_paths: list,
                                    features_destination_path: str,
                                    filter_old_rows: bool):
    """
    Function that generate reviews features for given books and writes them in numpy file
    :param book_reviews_csv_file_paths: file paths of reviews for books
    :param features_destination_path: destination path where we want to store the features
    :param filter_old_rows: generate features only for books added before variable YEAR

    :return:
    """
    old_features = {}

    model = SentenceTransformer('paraphrase-MiniLM-L6-v2')

    books_review = pd.concat((pd.read_csv(f) for f in book_reviews_csv_file_paths))
    if filter_old_rows:
        books_review = books_review.loc[books_review["year"] <= YEAR]
        features_destination_path = features_destination_path + "_old"
    else:
        old_features = load_books_features(features_destination_path + "_old.npy")
        books_review = books_review.loc[books_review["year"] > YEAR]
        features_destination_path = features_destination_path + "_new"

    book_reviews_data = books_review[['book_id', 'review_text']].groupby('book_id')['review_text'] \
        .agg(lambda x: list(x.astype(str))).to_dict()

    book_reviews_embeddings = {}
    for book, list_reviews in book_reviews_data.items():
        sentences = list(list_reviews)
        sentence_embeddings = model.encode(sentences)
        # average the review results
        sentence_embeddings = np.mean(sentence_embeddings, axis=0)
        if not filter_old_rows:
            if book in old_features:
                book_features = old_features[book]
                sentence_embeddings = np.mean(np.array([book_features, sentence_embeddings]), axis=0)

        book_reviews_embeddings.setdefault(book, sentence_embeddings)

    books_features = np.array(list(book_reviews_embeddings.items()), dtype=object)
    with open(features_destination_path + ".npy", 'wb') as file:
        np.save(file, books_features)


def generate_books_description_reviews_features(books_description_features: str,
                                                books_reviews_features: str,
                                                features_destination_path: str):
    """
    Function that loads all books description and reviews features, then averages all the vectors
    forming one final vector for each book

    :param books_description_features: file path
    :param books_reviews_features: file path
    :param features_destination_path: destination path where we want to store the features
    :return:
    """
    description_features = load_books_features(books_description_features)
    reviews_features = load_books_features(books_reviews_features)

    books_embeddings = {}
    for book_id in reviews_features.keys():
        book_description_features = description_features[book_id]
        book_reviews_features = reviews_features[book_id]
        features = np.mean(np.array([book_description_features, book_reviews_features]), axis=0)
        books_embeddings.setdefault(book_id, features)

    books_features = np.array(list(books_embeddings.items()), dtype=object)
    with open(features_destination_path, 'wb') as file:
        np.save(file, books_features)


def generate_user_description_features(books_csv_file_paths: list,
                                       books_description_features: str,
                                       features_destination_path: str,
                                       filter_old_rows: bool):
    """
    Function that generates description features for each user.
    Features in this function are generated in the following way:
        1. Gather all the books that a user reviewed/read
        2. Load description features for each book
        3. For each book that a user reviewed/read, collect all the vectors
        4. For each user, the resulting features are average from all their reviewed/read books vectors


    :param books_csv_file_paths: file paths of reviews/interaction data for books
    :param books_description_features: file path
    :param features_destination_path: destination path where we want to store the features
    :param filter_old_rows: generate features only for books added before variable YEAR
    :return:
    """
    old_features = {}
    books = pd.concat((pd.read_csv(f) for f in books_csv_file_paths))

    if filter_old_rows:
        books = books.loc[books["year"] <= YEAR]
        features_destination_path = features_destination_path + "_old"
    else:
        old_features = load_users_features(features_destination_path + "_old.npy")
        books = books.loc[books["year"] > YEAR]
        features_destination_path = features_destination_path + "_new"

    books['book_id'] = books['book_id'].astype(str)

    # get all books that a user reviewed/read
    user_books = books[['user_id', 'book_id']] \
        .groupby('user_id')['book_id'] \
        .apply(list) \
        .to_dict()

    features = load_books_features(books_description_features)

    user_embeddings = dict()
    for user, books in user_books.items():
        features_array = list()
        for book in books:
            feature = features[book]
            features_array.append(feature)
        features_array = np.array(features_array)
        features_array = np.mean(features_array, axis=0)

        if not filter_old_rows:
            if user in old_features:
                user_features = old_features[user]
                features_array = np.mean(np.array([user_features, features_array]), axis=0)
        user_embeddings.setdefault(user, features_array)

    user_features = np.array(list(user_embeddings.items()), dtype=object)
    with open(features_destination_path + ".npy", 'wb') as file:
        np.save(file, user_features)


def generate_user_description_reviews_features(books_csv_file_paths: list,
                                               books_description_reviews_features: str,
                                               features_destination_path: str,
                                               filter_old_rows: bool):
    """
    Function that generates description and reviews features for each user.
    Features in this function are generated in the following way:
        1. Gather all the books that a user reviewed/read
        2. Load description and reviews features for each book
        3. For each book that a user reviewed/read, collect all the vectors
        4. For each user, the resulting features are average from all their reviewed/read books vectors

    :param books_csv_file_paths: file paths of reviews/interaction data for books
    :param books_description_reviews_features: file path
    :param features_destination_path: destination path where we want to store the features
    :param filter_old_rows: generate features only for books added before variable YEAR
    :return:
    """

    books_reviews = pd.concat((pd.read_csv(f) for f in books_csv_file_paths))

    if filter_old_rows:
        books_reviews = books_reviews.loc[books_reviews["year"] <= YEAR]
        features_destination_path = features_destination_path + "_old"
    else:
        old_features = load_users_features(features_destination_path + "_old.npy")
        books_reviews = books_reviews.loc[books_reviews["year"] > YEAR]
        features_destination_path = features_destination_path + "_new"

    books_reviews['book_id'] = books_reviews['book_id'].astype(str)

    # get all books that a user reviewed/read
    user_books = books_reviews[['user_id', 'book_id']] \
        .groupby('user_id')['book_id'] \
        .apply(list) \
        .to_dict()

    features = load_books_features(books_description_reviews_features)

    user_embeddings = {}
    for user, books in user_books.items():
        features_array = list()
        for book in books:
            if book in features:
                feature = features[book]
                features_array.append(feature)

        if len(features_array)==0:
            continue
        features_array = np.array(features_array)
        features_array = np.mean(features_array, axis=0)

        if not filter_old_rows:
            if user in old_features:
                user_features = old_features[user]
                features_array = np.mean(np.array([user_features, features_array]), axis=0)

        user_embeddings.setdefault(user, features_array)

    user_features = np.array(list(user_embeddings.items()), dtype=object)
    with open(features_destination_path + ".npy", 'wb') as file:
        np.save(file, user_features)
