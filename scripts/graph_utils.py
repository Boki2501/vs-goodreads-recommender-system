import pandas as pd
import networkx as nx
from scripts.node_features_utils import load_books_features, load_users_features
import itertools

YEAR = 2015
FEATURES_SIZE = 384
similar_books_edge_list_file_path = "data/similar_books_edgelist.txt"


def generate_edge_list_from_interactions(interactions_file_paths: list,
                                         edge_list_destination_file_path: str,
                                         filter_old_rows: str):
    """
        Function that reads interactions file and generate edge list

        :param interactions_file_paths: list of interactions file locations
        :param edge_list_destination_file_path: edge list destination file location
        :param filter_old_rows: generate features only for books added before variable YEAR
        :return:
        """

    books_interactions = pd.concat((pd.read_csv(f) for f in interactions_file_paths))

    if filter_old_rows:
        books_interactions = books_interactions.loc[books_interactions["year"] <= YEAR]
        edge_list_destination_file_path = edge_list_destination_file_path + "_old"
    else:
        books_interactions = books_interactions.loc[books_interactions["year"] > YEAR]
        edge_list_destination_file_path = edge_list_destination_file_path + "_new"

    user_books = books_interactions[["user_id", "book_id"]]

    output_string_list = list()

    for index, row in user_books.iterrows():
        edge = f"{row['user_id']}\t{row['book_id']}\n"
        output_string_list.append(edge)

    with open(edge_list_destination_file_path + ".txt", 'w') as file:
        file.write(''.join(output_string_list))


def generate_edge_list_from_reviews(reviews_file_paths: list,
                                    edge_list_destination_file_path: str,
                                    weights_type: int,
                                    filter_old_rows: str):
    """
    Function that reads reviews file and generate edge list

    :param reviews_file_paths: list of reviews file locations
    :param edge_list_destination_file_path: edge list destination file location
    :param weights_type: 1 - ratings weights
                         2 - reviews helpfulness weights
    :param filter_old_rows: generate features only for books added before variable YEAR

    :return:
    """
    books_reviews = pd.concat((pd.read_csv(f) for f in reviews_file_paths))

    if filter_old_rows:
        books_reviews = books_reviews.loc[books_reviews["year"] <= YEAR]
        edge_list_destination_file_path = edge_list_destination_file_path + "_old"
    else:
        books_reviews = books_reviews.loc[books_reviews["year"] > YEAR]
        edge_list_destination_file_path = edge_list_destination_file_path + "_new"

    if weights_type == 1:
        weights = 'rating'
    else:
        weights = 'helpfulness'

    user_books = books_reviews[["user_id", "book_id", weights]]

    output_string_list = list()

    for index, row in user_books.iterrows():
        edge = f"{row['user_id']}\t{row['book_id']}\t{row[weights]}\n"
        output_string_list.append(edge)

    with open(edge_list_destination_file_path + ".txt", 'w') as file:
        file.write(''.join(output_string_list))


def generate_edge_list_from_similar_books(books_file_paths: list, edge_list_destination_file_path: str):
    """
    Function that generates edge list for the similar books

    :param books_file_paths: list of book file locations
    :param edge_list_destination_file_path: edge list destination file location
    :return:
    """

    books_data = pd.concat((pd.read_csv(f) for f in books_file_paths))

    books = books_data[["book_id", "similar_books"]]
    books_id = books["book_id"].astype(str).unique()
    output_string_list = list()

    for index, row in books.iterrows():
        source_book_id = row['book_id']

        similar_books_string = row['similar_books'].replace("[", "").replace("]", "") \
            .replace(" ", "") \
            .replace("'", "")

        if similar_books_string == "" or not similar_books_string:
            continue

        similar_books = similar_books_string.split(",")

        for book in similar_books:
            if book not in books_id:
                continue
            book = book.replace("'", "")
            edge = f"{source_book_id}\t{book}\n"
            output_string_list.append(edge)

    with open(edge_list_destination_file_path, 'w') as file:
        file.write(''.join(output_string_list))


def read_edge_list(edge_list_file_path: str, with_weights: bool):
    """
    Function that loads edge list from a file

    :param edge_list_file_path: file location where the edge list is saved
    :param with_weights: read edge list with weights

    :return: list of edges that form a graph
    """
    edge_list = list()

    with open(edge_list_file_path, "r") as file:
        file.readline()
        for line in file.readlines():
            line = line.replace("\n", "")
            values = tuple(line.split("\t"))
            source = values[0]
            target = values[1]
            if with_weights:
                weight = int(values[2])
                edge_list.append((source, target, weight))
            else:
                edge_list.append((source, target))
    return edge_list


def read_graph(edge_list_filepath: str,
               books_features_file_path: str,
               users_features_file_path: str,
               with_weights: bool,
               similar_books_edges: bool):
    """
    Function that creates graph from edge list, data frame filled with nodes and their features
    ex. -Reviews files are with weights
        -Interactions files are without weights

    :param edge_list_filepath: file location where the edge list is saved
    :param books_features_file_path: file path where the books features are saved
    :param users_features_file_path: file path where the users features are saved
    :param with_weights: read graph with weights
    :param similar_books_edges: add edges between similar books


    :return: graph, nodes data frame and features_names
    """

    books_features = load_books_features(books_features_file_path)
    user_features = load_users_features(users_features_file_path)

    nodes = {**user_features, **books_features}
    feature_names = ["f_{}".format(ii) for ii in range(FEATURES_SIZE)]

    edge_list = read_edge_list(edge_list_filepath, with_weights)
    edge_list = [edge for edge in edge_list if edge[0] in user_features.keys() and edge[1] in books_features.keys()]
    g = nx.Graph()
    g.add_nodes_from(list(books_features.keys()), label='book')
    g.add_nodes_from(list(user_features.keys()), label='user')
    nx.set_node_attributes(g, nodes, "feature")
    if with_weights:
        g.add_weighted_edges_from(edge_list, label="rating")
    else:
        g.add_edges_from(edge_list, label="interested")

    if similar_books_edges:
        similar_books_edge_list = read_edge_list(similar_books_edge_list_file_path, with_weights=False)

        # filter out the books that doesn't have features
        similar_books_edge_list = [edge for edge in similar_books_edge_list if \
                                   edge[0] in books_features.keys() and edge[1] in books_features.keys()]
        # new_books = list(set(itertools.chain.from_iterable(similar_books_edge_list)))
        # g.add_nodes_from(new_books, label='book')
        g.add_edges_from(similar_books_edge_list, label="similar")

    return g, nodes, feature_names
